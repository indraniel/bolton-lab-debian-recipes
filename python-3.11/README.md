# Production mode

    docker build -t python-3.11:v1 .
    docker run -i -t -v $PWD:/release --rm python-3.11:v1

# Development Mode

comment out the "COPY" commands in the Dockerfile, and then run:

    docker build -t python-3.11:v1 .
    docker run -i -t -v $PWD:/build --rm python-3.11:v1 bash

# Testing

    docker build -t python-3.11:v1 .
    docker run -i -t -v $PWD:/release --rm python-3.11:v1 bash

    # inside the container
    cd /release
    # manually install the prerequisites for the package
    apt-get install make libncurses6 libreadline8 libssl1.1 libdb5.3 libc6 libsqlite3-0 sqlite3 libbz2-1.0 zlib1g libffi7 libffi-dev
    dpkg --install bolton-lab-python-3.11.0_3.11.0-1debian11.6.deb
