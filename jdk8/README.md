# production mode

    docker build -t jdk8:v1 .
    docker run -i -t -v $PWD:/release --rm jdk8:v1

# Development Mode

comment out the "COPY" commands in the Dockerfile, and then run:

    docker build -t jdk8:v1 .
    docker run -i -t -v $PWD:/build --rm jdk8:v1 bash

# Testing

    docker build -t jdk8:v1 .
    docker run -i -t -v $PWD:/release --rm jdk8:v1 bash

    # inside the container
    cd /release
    dpkg --install bolton-lab-openjdk-8u362_8u362-1debian11.6.deb
