# production mode

    docker build -t jdk17:v1 .
    docker run -i -t -v $PWD:/release --rm jdk17:v1

# Development Mode

comment out the "COPY" commands in the Dockerfile, and then run:

    docker build -t jdk17:v1 .
    docker run -i -t -v $PWD:/build --rm jdk17:v1 bash

# Testing

    docker build -t jdk17:v1 .
    docker run -i -t -v $PWD:/release --rm jdk17:v1 bash

    # inside the container
    cd /release
    dpkg --install bolton-lab-openjdk-17.0.6b10_17.0.6b10-1debian11.6.deb

# GitHub Sources

* https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.6%2B10/OpenJDK17U-jdk_x64_linux_hotspot_17.0.6_10.tar.gz
* https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.6%2B10/OpenJDK17U-static-libs-glibc_x64_linux_hotspot_17.0.6_10.tar.gz
