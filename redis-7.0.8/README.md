# production mode

    docker build -t redis7.0:v1 .
    docker run -i -t -v $PWD:/release --rm redis7.0:v1

# Development Mode

comment out the "COPY" commands in the Dockerfile, and then run:

    docker build -t redis7.0:v1 .
    docker run -i -t -v $PWD:/build --rm redis7.0:v1 bash

# Testing

    docker build -t redis7.0:v1 .
    docker run -i -t -v $PWD:/release --rm redis7.0:v1 bash

    # inside the container
    cd /release

    # manually install the prerequisites for the package
    apt-get install libc6 libcurl4 ca-certificates libssl1.1 libsystemd0 systemd
    
    # actually install the pacakge of interest
    dpkg --install bolton-lab-redis-7.0.8_7.0.8-1debian11.6.deb

# Sources

* https://download.redis.io/redis-stable.tar.gz
