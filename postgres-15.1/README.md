# production mode

    docker build -t postgres15.1:v1 .
    docker run -i -t -v $PWD:/release --rm postgres15.1:v1

# Development Mode

comment out the "COPY" commands in the Dockerfile, and then run:

    docker build -t postgres15.1:v1 .
    docker run -i -t -v $PWD:/build --rm postgres15.1:v1 bash

# Testing

    docker build -t postgres15.1:v1 .
    docker run -i -t -v $PWD:/release --rm postgres15.1:v1 bash

    # inside the container
    cd /release

    # manually install the prerequisites for the package
    apt-get install libc6 libreadline8 libssl1.1 zlib1g liblz4-1 libzstd1 libicu67 bolton-lab-python-3.11.1
    
    # actually install the pacakge of interest
    dpkg --install bolton-lab-postgres-15.1_15.1-1debian11.6.deb

# Sources

* https://download.redis.io/redis-stable.tar.gz
