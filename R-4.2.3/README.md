# production mode

    docker build -t r:v1 .
    docker run -i -t -v $PWD:/release --rm r:v1

# Development Mode

comment out the "COPY" commands in the Dockerfile, and then run:

    docker build -t r:v1 .
    docker run -i -t -v $PWD:/build --rm r:v1 bash

# Testing

    docker build -t r:v1 .
    docker run -i -t -v $PWD:/release --rm r:v1 bash

    # inside the container
    cd /release
    # manually install the prerequisites for the package
    apt-get install build-essential g++ gfortran gfortran-10 libncurses6 libreadline8 libjpeg62-turbo libpcre2-16-0 libpng16-16 zlib1g zip unzip libbz2-1.0 libc6 libgomp1 liblzma5 libtiff5 libx11-6 libxt6 libxmu6 libcairo2 libpango1.0-0 libpangocairo-1.0-0 libicu67 libcurl4 libcurl4-openssl-dev curl bolton-lab-openblas-0.3.23 pkg-config

    dpkg --install bolton-lab-r-4.2.3_4.2.3-1debian11.7.deb
