# production mode

    docker build -t rocksdb7.9:v1 .
    docker run -i -t -v $PWD:/release --rm rocksdb7.9:v1

# Development Mode

comment out the "COPY" commands in the Dockerfile, and then run:

    docker build -t rocksdb7.9:v1 .
    docker run -i -t -v $PWD:/build --rm rocksdb7.9:v1 bash

# Testing

    docker build -t rocksdb7.9:v1 .
    docker run -i -t -v $PWD:/release --rm rocksdb7.9:v1 bash

    # inside the container
    cd /release

    # manually install the prerequisites for the package
    apt-get install libc6 libstdc++6 libjemalloc2 zlib1g libbz2-1.0 liblz4-1 libsnappy1v5 libzstd1
    
    # actually install the pacakge of interest
    dpkg --install bolton-lab-rocksdb-7.9.2_7.9.2-1debian11.6.deb

# Sources

* https://download.rocksdb.io/rocksdb-stable.tar.gz
