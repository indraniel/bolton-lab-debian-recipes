.PHONY: clean debian debclean

# external commands used
CURL  := /usr/bin/curl
TAR   := /bin/tar
CP    := /bin/cp
RM    := /bin/rm
MV    := /bin/mv
MKDIR := /bin/mkdir
ECHO  := /bin/echo
CHMOD := /bin/chmod
AR    := /usr/bin/ar
TEST  := /usr/bin/test

# basic workspace directories
WORK_DIR         := /build
BASE_INSTALL_DIR := $(WORK_DIR)
RESOURCES_DIR    := $(WORK_DIR)/resources
RELEASE_DIR      := /release

# source code information
ROCKSDB_VERSION     := 7.9.2
ROCKSDB_URL         := https://github.com/facebook/rocksdb/archive/refs/tags/v7.9.2.tar.gz
ROCKSDB_TGZ         := $(RESOURCES_DIR)/v$(ROCKSDB_VERSION).tar.gz
ROCKSDB_OUTPUT_DIR  := $(WORK_DIR)/rocksdb-$(ROCKSDB_VERSION)
ROCKSDB             := $(ROCKSDB_OUTPUT_DIR)/include/rocksdb/version.h

# debian packaging related variables
DEB_BUILD_DIR       := $(WORK_DIR)/deb-build
DEBIAN_EDITION      := $(shell cat /etc/debian_version)
DEB_RELEASE_VERSION := 1debian$(DEBIAN_EDITION)
DEB_PKG             := bolton-lab-rocksdb-$(ROCKSDB_VERSION)_$(ROCKSDB_VERSION)-$(DEB_RELEASE_VERSION).deb
DEB_PKG_PATH        := $(RELEASE_DIR)/$(DEB_PKG)
DEB_BASE_INSTALL    := /opt/bolton-lab/rocksdb-$(ROCKSDB_VERSION)

# DEBIAN CONTROL FILE ##########################################################
define debian_control
Package: bolton-lab-rocksdb-$(ROCKSDB_VERSION)
Architecture: amd64
Section: science
Maintainer: Indraniel Das <idas@wustl.edu>
Priority: optional
Depends: libc6, libstdc++6, libjemalloc2, zlib1g, libbz2-1.0, liblz4-1, libsnappy1v5, libzstd1
Description: rocksdb for Bolton Lab ( $(ROCKSDB_VERSION) )
Version: $(ROCKSDB_VERSION)-$(DEB_RELEASE_VERSION)
endef
export debian_control

# DEBIAN POSTRM FILE ###########################################################
define debian_postrm
#!/bin/bash

BASE=$(DEB_BASE_INSTALL)

if [ -e $${BASE} ]; then
    $(RM) -rfv $${BASE}
fi
endef
export debian_postrm

all: debian

debian: | $(ROCKSDB)
	# setup the directory
	$(TEST) -d $(DEB_BUILD_DIR) || $(MKDIR) $(DEB_BUILD_DIR)
	
	# setup the debian package meta information
	$(ECHO) "$$debian_postrm" > $(DEB_BUILD_DIR)/postrm
	$(ECHO) "$$debian_control" > $(DEB_BUILD_DIR)/control
	$(ECHO) 2.0 > $(DEB_BUILD_DIR)/debian-binary
	
	# create the "installed" file directory structure
	$(MKDIR) -p $(DEB_BUILD_DIR)/$(DEB_BASE_INSTALL)
	
	# install the untarred file for jdk
	$(CP) -rv $(ROCKSDB_OUTPUT_DIR)/* $(DEB_BUILD_DIR)/$(DEB_BASE_INSTALL)
	
	# create the underlying tars of the debian package
	$(TAR) cvzf $(DEB_BUILD_DIR)/data.tar.gz --owner=0 --group=0 -C $(DEB_BUILD_DIR) opt
	$(TAR) cvzf $(DEB_BUILD_DIR)/control.tar.gz -C $(DEB_BUILD_DIR) control postrm
	
	# assemble the formal "deb" package
	cd $(DEB_BUILD_DIR) && \
		$(AR) rc $(DEB_PKG) debian-binary control.tar.gz data.tar.gz && \
		$(MV) $(DEB_PKG) $(RELEASE_DIR)

$(ROCKSDB): $(ROCKSDB_TGZ)
	$(TAR) -C $(WORK_DIR) -zxvf $(ROCKSDB_TGZ) && \
		mv rocksdb-$(ROCKSDB_VERSION) rocksdb-build && \
		cd rocksdb-build && \
		make -j 2 static_lib && \
		make install-static PREFIX=$(ROCKSDB_OUTPUT_DIR) && \
		make clean && \
		make -j 2 shared_lib && \
		make install-shared PREFIX=$(ROCKSDB_OUTPUT_DIR) && \
		make install-headers PREFIX=$(ROCKSDB_OUTPUT_DIR)

$(ROCKSDB_TGZ):
	mkdir -p $(RESOURCES_DIR)
	cd $(RESOURCES_DIR) && \
		$(CURL) -L -O $(ROCKSDB_URL)

debclean:
	if [ -e $(DEB_BUILD_DIR) ]; then $(RM) -rf $(DEB_BUILD_DIR); fi
	if [ -e $(DEB_PKG_PATH) ]; then $(RM) -rf $(DEB_PKG_PATH); fi

clean:
	if [ -e $(ROCKSDB_OUTPUT_DIR) ]; then $(RM) -rf $(ROCKSDB_OUTPUT_DIR); fi
	if [ -e $(DEB_BUILD_DIR) ]; then $(RM) -rf $(DEB_BUILD_DIR); fi
	if [ -e $(DEB_PKG_PATH) ]; then $(RM) -rf $(DEB_PKG_PATH); fi

