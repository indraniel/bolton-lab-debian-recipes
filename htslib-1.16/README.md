# production mode

    docker build -t htslib-1.16-build:v1 .
    docker run -i -t -v $PWD:/release --rm htslib-1.16-build:v1

# Development Mode

comment out the "COPY" commands in the Dockerfile AND uncomment the VOLUME /build command, and then run:

    docker build -t htslib-1.16-build:v1 .
    docker run -i -t -v $PWD:/build --rm htslib-1.16-build:v1 bash

# Testing

    docker build -t htslib-1.16-build:v1 .
    docker run -i -t -v $PWD:/release --rm htslib-1.16-build:v1 bash

    # inside the container
    cd /release
    # manually install the prerequisites for the package
    apt-get install libc6 libcurl4 zlib1g ca-certificates libbz2-1.0 liblzma5 libssl1.1
    # actually install the pacakge of interest
    dpkg --install bolton-lab-htslib-1.16_1.16-1debian11.6.deb
