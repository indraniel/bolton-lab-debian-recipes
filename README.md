# Debian Recipes

A collection of debian package recipes for Kelly Bolton's Lab at WashU Division of Oncology.

_The recipes are targeting [Debian 11 (Bullseye)](https://www.debian.org)._
